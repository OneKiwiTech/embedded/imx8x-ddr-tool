# Board Bring-Up

## 1. DDR Stress Test Script

1. tạo file **mx8dxp_c0_b0_lpddr4_1200mhz_RPAv16_onekiwi.xlsx** từ **MX8QXP_C0_B0_LPDDR4_RPA_1.2GHz_v16.xlsx**
2. chỉnh thông số cấu hình RAM trong file **mx8dxp_c0_b0_lpddr4_1200mhz_RPAv16_onekiwi.xlsx**. trong 2 sheet **Register Configuration** và **BoardDataBusConfig**
3. sau khi chỉnh thông số xong Excel tự động cập nhập thông số trong 2 sheet **DCD CFG file CBT** và **DDR Stress Test Script CBT**
4. sheet **DDR Stress Test Script CBT** dùng để Load DDR Script trong phần mềm **NXP i.MX8-SCU DDR Test Tool**
5. sheet **DCD CFG file CBT** dùng để cấu hình trong **imx-sc-firmware**
6. chạy file **mx8_ddr_stress_test_ER15_installation.exe**. xong chạy file **MX8_DDR_Tester.exe**
7. khi chạy file MX8_DDR_Tester.exe, sẽ có giao diện như hình bên dưới:

  ![NXP i.MX8-SCU DDR Test Tool](img/imx8-ddr-test-tool.jpg "NXP i.MX8-SCU DDR Test Tool")

8. trên board i.MX8X dùng **UART0** để kết Debug UART
9. nhấn **Load DDR Script** sau đó chọn tới file được tạo ra từ sheet **DDR Stress Test Script CBT**
10. sau đó nhấn **Download** rồi **Stress Test**

## 2. Build i.MX-System-Controller-Firmware
- https://github.com/OneKiwiEmbedded/imx-sc-firmware
1. vào thư mục **imx-sc-firmware/src/scfw_export_mx8qx_b0/platform/board**
2. copy thư mục **mx8qx_mek** thành **mx8qx_onekiwi**
3. vào tthư mục **dcd** trong thư mục **mx8qx_onekiwi** tạo file **imx8dx_dcd_lpddr4_16bit_1.2GHz_onekiwi.cfg** với nội dung là trong sheet **DCD CFG file CBT**
4. mở file **Makefile** chỉnh dòng *DDR_CON ?= imx8qx_dcd_1.2GHz* thành *DDR_CON ?= imx8dx_dcd_lpddr4_16bit_1.2GHz_onekiwi*

## 3. Build ARM Trusted firmware
- `git clone https://github.com/nxp-imx/imx-atf.git -b lf_v2.8`

## 4. Build u-boot
- `git clone https://github.com/OneKiwiEmbedded/uboot-imx.git -b onekiwi-lf_v2023.04`

## 5. Build kernel
- `git clone https://github.com/OneKiwiEmbedded/linux-imx.git -b onekiwi-lf-6.1.y`

## 6.Build yocto
- `repo init -u https://github.com/OneKiwiEmbedded/imx-manifest -b onekiwi -m imx-6.1.55-2.2.0.xml`
- `repo sync`
- `MACHINE=imx8qxp-onekiwi DISTRO=fsl-imx-xwayland source ./imx-setup-release.sh -b build`
- `bitbake imx-image-multimedia --runall=fetch`
- `bitbake imx-image-multimedia`